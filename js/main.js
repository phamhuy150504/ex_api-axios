const $ = document.querySelector.bind(document);
const $$ = document.querySelectorAll.bind(document);
let data = []
const URL = 'https://63bea7fff5cfc0949b5d497c.mockapi.io';
function fetAPI() {
    axios({
        url: `${URL}/Manager`,
        method: 'GET'
    }).then(res => {
        RenderListUsers(res.data)
        data.push(...res.data);
    }).catch((err = '404') => {
        console.log(err);
    });
};

// call API 
fetAPI();

// Reset User Profile
function putUser() {
    let data = testValueReset();
    if (data) {
        turnOnLoading();
        return axios({
            url: `${URL}/Manager/${data.id}`,
            method: 'PUT',
            data: data
       }).then(res => {
            turnOffLoading();
            fetAPI();
         }).catch((err = '404') => {
            console.log(err);
        });
    } else {
        turnOffLoading();
        return ;
    }
}

