const Validate = new Validation();

function turnOnLoading() {
    $('#loading').style = 'display: flex';
 };

function turnOffLoading() {
    $('#loading').style = `display: none`;
 };


//  close form
function btnAdd() {
    const form = $('#form__best')
    let test = form.classList.contains('d-none')
    if(!test) {
        form.classList.add("d-none");
    } else {
        form.classList.remove("d-none");
        deleteValueInPut();
        callModal('ADD USER', true, 1)
        $('#idUser').disabled = false;
    }
    $('.fa').onclick = function () {
        form.classList.add("d-none");
        deleteValueInPut();
    };  

 };
 
callModal = (modal_title, readonly, type) => {
    $('#a').innerHTML = modal_title;
    $('#idUser').readonly = readonly;

    switch(type) {
        case 1: 
            $('#btn-form').style = `display: block`;
            $('#btn-reset').style = `display: none`;
            break;
        case 2: 
            $('#btn-form').style = `display: none`;
            $('#btn-reset').style = `display: block`;
            break;
    }
}

deleteValueInPut = () => {
    let elmInput = $$('.form-control')
    for (let element of elmInput) {
        element.value = '';
    }
    const elmErr = $$('.notification__errForm')
    for (let elm of elmErr) {
        elm.innerHTML = ``;
    }
    $('#type').selectedIndex = 0;
    $('#language').selectedIndex = 0;

}

 function getInputForm() {
    let idUser = $('#idUser');
    let account  = $('#account');
    let name = $('#name');
    let password = $('#password');
    let email = $('#email');
    let typeUser = $('#type');
    let language = $('#language');
    let picAvar = $('#photo');
    let desg = $('#MoTa');
    return {
        idUser,
        account,
        name,
        password,
        email,
        typeUser,
        language,
        picAvar,
        desg
    };
 };

 function getValueInputForm() {
    let id = $('#idUser').value;
    let taiKhoan  = $('#account').value;
    let hoTen = $('#name').value;
    let matKhau = $('#password').value;
    let email = $('#email').value;
    let loaiND = $('#type').value;
    let ngonNgu = $('#language').value;
    let hinhAnh = $('#photo').value;
    let moTa = $('#MoTa').value;
    return {
        id,
        taiKhoan,
        hoTen,
        matKhau,
        email,
        loaiND,
        ngonNgu,
        hinhAnh,
        moTa
    };
 };

 
// handle reset form
function getInfoFromTable(data) {
    btnAdd();
    let inputForm = getInputForm();
    inputForm.idUser.value = data.id;
    inputForm.idUser.disabled = true;
    inputForm.account.value = data.taiKhoan;
    inputForm.name.value = data.hoTen;
    inputForm.password.value = data.matKhau;
    inputForm.email.value = data.email;
    inputForm.typeUser.value = data.loaiND;
    inputForm.language.value = data.ngonNgu;
    inputForm.picAvar.value = data.hinhAnh;
};

function handleUpdateUser() {
    const form = $('#form__best');
    let push = putUser()
    if (push) {
    form.classList.add("d-none");
    }
};

function testEmpty(valueIP, tagInput) {
   return  Validate.TestEmpty(valueIP, tagInput)
};

function testValidateInput() {
     // dom to INPUT form 
    let tagInput = getInputForm();
     // dom value to INPUT form
    let valueIP = getValueInputForm();
    let isValid = true;
    if (testEmpty(valueIP.id, tagInput.idUser)) {
        isValid = false;
    }  

    if (testEmpty(valueIP.taiKhoan, tagInput.account)) {
        isValid = false;
    } else if(Validate.noSame(data, valueIP.taiKhoan, tagInput.account)){
        isValid = false;
    } 

    if (testEmpty(valueIP.hoTen, tagInput.name)  ) {
        isValid = false;    
    } else if (Validate.testSpecialChars(valueIP.hoTen, tagInput.name)) {
        isValid = false; 
    } else if (Validate.testNumber(valueIP.hoTen, tagInput.name)) {
        isValid = false;
    }

    if (testEmpty(valueIP.matKhau, tagInput.password) ) {
        isValid = false;
    } else if (Validate.testFormatPassWord(valueIP.matKhau, tagInput.password)) {
        isValid = false;
    }
    if (testEmpty(valueIP.email, tagInput.email)) {
        isValid = false;
    } else if (Validate.testEmail(valueIP. email, tagInput.email)) {
        isValid = false;
    }
    if (testEmpty(valueIP.loaiND, tagInput.typeUser)) {
        isValid = false;
    }
    if (testEmpty(valueIP.ngonNgu, tagInput.language)) {
        isValid = false;
    }
    if (testEmpty(valueIP.hinhAnh, tagInput.picAvar)) {
        isValid = false;
    }
    if(testEmpty(valueIP.moTa, tagInput.desg)) {
        isValid = false;    
    } else if (Validate.testLength(valueIP.moTa, tagInput.desg)) {
        return false;
    }
    
    if (!isValid) {
        return;
    } else {
        return valueIP;
    }
};
function testValueReset() {
        // dom to INPUT form 
        let tagInput = getInputForm();
        // dom value to INPUT form
        let valueIP = getValueInputForm();
        let isValid = true;
        if (testEmpty(valueIP.id, tagInput.idUser)) {
           isValid = false;
        };  
   
        if (testEmpty(valueIP.taiKhoan, tagInput.account)) {
           isValid = false;
        } else if(Validate.noSameReset(data, valueIP.taiKhoan, tagInput.account, valueIP.id)){
            isValid = false;
        };
        if (testEmpty(valueIP.hoTen, tagInput.name)  ) {
           isValid = false;    
        } else if (Validate.testSpecialChars(valueIP.hoTen, tagInput.name)) {
           isValid = false; 
        } else if (Validate.testNumber(valueIP.hoTen, tagInput.name)) {
            isValid = false;
        }
   
        if (testEmpty(valueIP.matKhau, tagInput.password) ) {
           isValid = false;
        } else if (Validate.testFormatPassWord(valueIP.matKhau, tagInput.password)) {
           isValid = false;
        };
       
        if (testEmpty(valueIP.email, tagInput.email)) {
           isValid = false;
        } else if (Validate.testEmail(valueIP. email, tagInput.email)) {
           isValid = false;
        };

        if (testEmpty(valueIP.loaiND, tagInput.typeUser)) {
           isValid = false;
        };

        if (testEmpty(valueIP.ngonNgu, tagInput.language)) {
           isValid = false;
        };

        if (testEmpty(valueIP.hinhAnh, tagInput.picAvar)) {
           isValid = false;
        };

        if(testEmpty(valueIP.moTa, tagInput.desg)) {
           isValid = false;    
        } else if (Validate.testLength(valueIP.moTa, tagInput.desg)) {
            return false;
        };
       
        if (!isValid) {
           return;
        } else {
           return valueIP;
        };
}


// first check: function validition(value)  viet 1 ham value goi 2 kia 