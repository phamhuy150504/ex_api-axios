function Validation() {
    this.TestEmpty = function (value, tagInput) {
        let inputErr = tagInput.parentNode.querySelector('.notification__errForm');
        if (value.trim() === '' || value === '0') {
            inputErr.innerText = 'Trường này không được trống';
            inputErr.style.color = 'red';
            return true;
        } else {
            inputErr.innerText = '';
            return false;
        }
    };
    this.noSame = function (data, value, tagInput) { 
        let inputErr = tagInput.parentNode.querySelector('.notification__errForm');
        for(let i = 0; i < data.length; i++) {
            if(data[i].taiKhoan === value) {
                inputErr.innerText = 'Tài khoản đã tồn tại';
                inputErr.style.color ='red'; 
                return true;
            } else {
                inputErr.innerText = '';
                return false;
            }
        };
    };
    this.noSameReset = function (data, value, tagInput, id) { 
        let inputErr = tagInput.parentNode.querySelector('.notification__errForm');
        for(let i = 0; i < data.length; i++) {
            if (data[i].id !== id) { 
                console.log(data[i].id !== id);
                if(data[i].taiKhoan === value) {
                    inputErr.innerText = 'Tài khoản đã tồn tại';
                    inputErr.style.color ='red'; 
                    return true;
                } else {
                    inputErr.innerText = '';
                    return false;
                } 
            } else {
                inputErr.innerText = '';
                return false;
            }
        };
    };
    this.testSpecialChars = function (value, tagInput) {
        let inputErr = tagInput.parentNode.querySelector('.notification__errForm');
        const re = /[`!@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?~]/;
        if(re.test(value)) {
            inputErr.innerHTML = 'Hộ tên không được chứa kí tự đặc biệt';
            inputErr.style.color ='red'; 
            return true;
        } else {
            inputErr.innerHTML = ``;
            return false;
        }
      };
      this.testLength = function (value, tagInput, min , max) {
        let inputErr = tagInput.parentNode.querySelector('.notification__errForm');
        if (value.length < min || value.length > max) {
            inputErr.innerText = 'Độ dài mật khẩu từ 6-8 kí tự';
            inputErr.style.color ='red';
            return true;
        } else {
            inputErr.innerText = ``;
            return false;
        }
      };

      this.testFormatPassWord = function(value, tagInput) {
        let inputErr = tagInput.parentNode.querySelector('.notification__errForm');
        let re = /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[#$@!%&*?])[A-Za-z\d#$@!%&*?]{6,8}$/;
        if(!re.test(value)) {
            inputErr.style.color ='red';
            inputErr.innerHTML = `Gồm 1 kí tự hoa, 1 kí tự số, 1 kí tự đặc biết, độ dài 6-8 kí tự`;
            return true;

        } else {
            inputErr.innerHTML = ``;    
            return false;    
        }
      };
      this.testEmail = function(value, tagInput) { 
        let inputErr = tagInput.parentNode.querySelector('.notification__errForm');
        const re =
        /^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;
        if(!value.match(re)) 
        {
            inputErr.style.color ='red';
            inputErr.innerHTML = `Xin mời bạn nhập đúng email address`;
            return true;
        } else {
            inputErr.innerHTML = ``;
            return false;
        }
      };
      this.testNumber = function (value, tagInput) {
        let inputErr = tagInput.parentNode.querySelector('.notification__errForm');
        const re =  /^[a-zA-ZÀÁÂÃÈÉÊÌÍÒÓÔÕÙÚĂĐĨŨƠàáâãèéêìíòóôõùúăđĩũơƯĂẠẢẤẦẨẪẬẮẰẲẴẶẸẺẼỀỀỂẾưăạảấầẩẫậắằẳẵặẹẻẽềềểếỄỆỈỊỌỎỐỒỔỖỘỚỜỞỠỢỤỦỨỪễệỉịọỏốồổỗộớờởỡợụủứừỬỮỰỲỴÝỶỸửữựỳỵỷỹ\s\W|_]+$/;
        if(!re.test(value)) {
            inputErr.style.color ='red';
            inputErr.innerHTML = `Ho ten khong duoc nhap so`;
            return true;
        } else {
            inputErr.innerHTML = ``;
            return false;
        }
      };
      this.testLength = function (value, tagInput) { 
        let inputErr = tagInput.parentNode.querySelector('.notification__errForm');
        if(value.length > 60) {
            inputErr.style.color ='red';
            inputErr.innerHTML = `Độ dài desg dưới 60 kí tự`;
            return true;
        } else {
            inputErr.innerHTML = ``;
            return false;
        }
       }
};

